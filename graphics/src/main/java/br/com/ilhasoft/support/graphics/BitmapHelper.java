package br.com.ilhasoft.support.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import br.com.ilhasoft.support.core.helpers.IoHelper;

/**
 * Created by danielsan on 8/20/15.
 */
public final class BitmapHelper {

    private static final String TAG = "BitmapHelper";

    private BitmapHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @Nullable
    public static Bitmap getBitmapFromUri(Context context, Uri uri) {
        try {
            return BitmapHelper.getBitmapFromInputStream(context.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "getBitmapFromUri: ", e);
        }

        return null;
    }

    @Nullable
    public static Bitmap getBitmapFromInputStream(InputStream inputStream) {
        final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        if (bitmap != null) BitmapHelper.close(inputStream);
        return bitmap;
    }

    @Nullable
    public static Bitmap getScaledBitmapFromUri(Context context, Uri uri, int targetLength) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);

            BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
            bitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, bitmapFactoryOptions);
            int scaleFactor = Math.min(bitmapFactoryOptions.outWidth / targetLength,
                                       bitmapFactoryOptions.outHeight / targetLength);
            bitmapFactoryOptions.inJustDecodeBounds = false;
            bitmapFactoryOptions.inSampleSize = scaleFactor;
            bitmapFactoryOptions.inPurgeable = true;

            BitmapHelper.close(inputStream);
            inputStream = context.getContentResolver().openInputStream(uri);
            Bitmap bitmapOutput = BitmapFactory.decodeStream(inputStream, null, bitmapFactoryOptions);
            BitmapHelper.close(inputStream);

            return bitmapOutput;
        } catch (Exception e) {
            Log.e(TAG, "getScaledBitmapFromUri: ", e);
        }

        return null;
    }

    @Nullable
    public static byte[] getBytesFromBitmap(Bitmap bitmap, int targetSize) {
        int quality = 100;
        while (quality >= 50) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
                int size = byteArrayOutputStream.size() / 1024;
                if (size > targetSize && quality >= 60) {
                    quality -= 10;
                    continue;
                }
                return byteArrayOutputStream.toByteArray();
            } catch (Exception e) {
                Log.e(TAG, "getBytesFromBitmap: ", e);
            }
            quality -= 10;
        }

        return null;
    }

    @Nullable
    public static Bitmap getScaledBitmapFromFile(String path, int targetLength) {
        try {
            BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
            bitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, bitmapFactoryOptions);
            int scaleFactor = Math.min(bitmapFactoryOptions.outWidth / targetLength,
                                       bitmapFactoryOptions.outHeight / targetLength);
            bitmapFactoryOptions.inJustDecodeBounds = false;
            bitmapFactoryOptions.inSampleSize = scaleFactor;
            bitmapFactoryOptions.inPurgeable = true;
            return BitmapFactory.decodeFile(path, bitmapFactoryOptions);
        } catch (Exception e) {
            Log.e(TAG, "getScaledBitmapFromFile: ", e);
        }

        return null;
    }

    public static Bitmap rotateBitmapIfNeeded(Bitmap bitmap, File pictureFile) throws IOException {
        ExifInterface exifInterface = new ExifInterface(pictureFile.getPath());
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateBitmap(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateBitmap(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateBitmap(bitmap, 270);
        }
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Bitmap getThumbnailFromVideoUri(Context context, Uri uri) throws URISyntaxException {
        String videoPath = IoHelper.getFilePathForUri(context, uri);
        return ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND);
    }

    public static FileDescriptor getFileDescriptorFromUri(Context context, Uri imageUri) {
        try {
            final ParcelFileDescriptor parcelFileDescriptor;
            parcelFileDescriptor = context.getContentResolver().openFileDescriptor(imageUri, "r");
            if (parcelFileDescriptor != null) return parcelFileDescriptor.getFileDescriptor();
        } catch (Exception e) {
            Log.e(TAG, "getFileDescriptorFromUri: ", e);
        }
        return null;
    }

    public static void recycle(final Bitmap bitmap) {
        if (!bitmap.isRecycled()) bitmap.recycle();
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e(TAG, "close: ", e);
            }
        }
    }

}
