package br.com.ilhasoft.support.graphics.drawable.badge;

import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;

/**
 * Created by daniel on 24/01/17.
 */
public interface Badge {

    /**
     * Get the badge text displayed.
     *
     * @return The badge text displayed, or {@code null}
     */
    @Nullable
    String getBadge();

    /**
     * Set the badge text to display.
     * @param badge The text to display, or {@code null} to remove the
     *              existing badge text
     */
    void setBadge(@Nullable String badge);

    /**
     * Set the badge number to display.
     *
     * @param badgeNumber The number to display
     */
    void setBadgeNumber(int badgeNumber);

    /**
     * Set the badge color.
     * @param badgeColor The badge color;
     */
    void setBadgeColor(@ColorInt int badgeColor);

    /**
     * Set the badge text color.
     *
     * @param badgeTextColor The badge text color;
     */
    void setBadgeTextColor(@ColorInt int badgeTextColor);

    /**
     * Set the badge text size.
     *
     * @param badgeTextSize The badge text size in pixels;
     */
    void setBadgeTextSize(float badgeTextSize);

}
