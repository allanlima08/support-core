package br.com.ilhasoft.support.graphics.exif;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by danielsan on 12/01/17.
 * Reference: http://stackoverflow.com/questions/27932146/android-picasso-auto-rotates-image
 */
public class ExifFixerInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Response response = chain.proceed(chain.request());
        final ResponseBody newBody = ResponseBody.create(response.body().contentType(),
                                                         processImage(response.body().bytes()));
        return response.newBuilder().body(newBody).build();
    }

    private byte[] processImage(byte[] imageBytes) {
        final int orientation = ExifHelper.getOrientation(imageBytes);

        if (orientation != 0) {
            Bitmap bmp = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            rotateImage(orientation, bmp).compress(Bitmap.CompressFormat.PNG, 100, stream);

            return stream.toByteArray();
        }
        return imageBytes;
    }

    private Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
    }

}
