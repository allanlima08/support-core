package br.com.ilhasoft.support.graphics;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import br.com.ilhasoft.support.core.helpers.IoHelper;

/**
 * Created by johncordeiro on 03/09/15.
 */
public final class BitmapCompressor {

    private static final int INVALID_VALUE = -1;
    private static final int DEFAULT_QUALITY = 100;

    private BitmapCompressor() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static File compressFile(File pictureFile, int quality) throws IOException {
        return compressFile(pictureFile, INVALID_VALUE, INVALID_VALUE, quality);
    }

    public static File compressFile(File pictureFile, int maxWidth, int maxHeight) throws IOException {
        return compressFile(pictureFile, maxWidth, maxHeight, DEFAULT_QUALITY);
    }

    public static File compressFile(File pictureFile, float maxWidth, float maxHeight, int quality) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(pictureFile.getPath());

        if (maxWidth != INVALID_VALUE && maxHeight != INVALID_VALUE) {
            bitmap = scaleBitmap(bitmap, maxWidth, maxHeight);
        }
        bitmap = BitmapHelper.rotateBitmapIfNeeded(bitmap, pictureFile);

        return setBitmapToNewFileCompressed(bitmap, IoHelper.createImageFilePathPublic(), quality);
    }

    public static File setBitmapToNewFileCompressed(Bitmap bitmap, File createdFile, int quality) throws IOException {
        final FileOutputStream fileOutputStream = new FileOutputStream(createdFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        return createdFile;
    }

    private static Bitmap scaleBitmap(Bitmap bitmap, float maxWidth, float maxHeight) {
        final float scale = Math.min((maxWidth / bitmap.getWidth()), (maxHeight / bitmap.getHeight()));
        final Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

}
