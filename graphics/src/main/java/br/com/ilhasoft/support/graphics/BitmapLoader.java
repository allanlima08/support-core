package br.com.ilhasoft.support.graphics;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import br.com.ilhasoft.support.graphics.tasks.AsyncDrawable;
import br.com.ilhasoft.support.graphics.tasks.BitmapFileWorkerTask;
import br.com.ilhasoft.support.graphics.tasks.BitmapUriWorkerTask;
import br.com.ilhasoft.support.graphics.tasks.BitmapWorkerTask;
import br.com.ilhasoft.support.graphics.tasks.VideoUriBitmapTask;

/**
 * Created by johndalton on 12/07/14.
 */
public final class BitmapLoader {

    private BitmapLoader() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void loadBitmapByFile(ImageView imageView, String pathName, int size) {
        if (cancelPotentialWork(pathName, imageView)) {
            final BitmapFileWorkerTask task = new BitmapFileWorkerTask(imageView, size);
            final AsyncDrawable<BitmapFileWorkerTask> asyncDrawable = new AsyncDrawable<>(imageView.getResources(), null, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(pathName);
        }
    }

    public static void loadBitmapByVideoPath(ImageView imageView, Uri uri, int size) {
        if (cancelPotentialWork(uri.getPath(), imageView)) {
            final VideoUriBitmapTask task = new VideoUriBitmapTask(imageView.getContext(), imageView, size);
            final AsyncDrawable<VideoUriBitmapTask> asyncDrawable = new AsyncDrawable<>(imageView.getResources(), null, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(uri);
        }
    }

    public static void loadBitmapByUri(ImageView imageView, Uri uri, int size) {
        if (cancelPotentialWork(uri.getPath(), imageView)) {
            final BitmapUriWorkerTask task = new BitmapUriWorkerTask(imageView.getContext(), imageView, size);
            final AsyncDrawable<BitmapUriWorkerTask> asyncDrawable = new AsyncDrawable<>(imageView.getResources(), null, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(uri);
        }
    }

    public static boolean cancelPotentialWork(String pathName, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.pathName;
            if (!bitmapData.equals(pathName)) {
                bitmapWorkerTask.cancel(true);
            } else {
                return false;
            }
        }
        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                try {
                    final AsyncDrawable<BitmapWorkerTask> asyncDrawable = (AsyncDrawable<BitmapWorkerTask>) drawable;
                    return asyncDrawable.getWorkerTask();
                } catch (Exception exception) { exception.printStackTrace(); }
            }
        }
        return null;
    }

}
