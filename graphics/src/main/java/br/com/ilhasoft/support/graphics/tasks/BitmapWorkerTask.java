package br.com.ilhasoft.support.graphics.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import br.com.ilhasoft.support.core.animation.MediaAlphaAnimator;

/**
 * Created by johndalton on 12/07/14.
 */
public abstract class BitmapWorkerTask<Input> extends AsyncTask<Input, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;

    protected final int size;
    public String pathName = null;

    public BitmapWorkerTask(ImageView imageView, int size) {
        imageViewReference = new WeakReference<>(imageView);
        this.size = size;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
                MediaAlphaAnimator.showMedia(imageView);
            }
        }
    }

}
