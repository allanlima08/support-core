package br.com.ilhasoft.support.graphics;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.widget.ImageView;

/**
 * @deprecated Use {@link br.com.ilhasoft.support.graphics.transformations.BlurEffectBitmapTransformation} instead.
 *
 * Created by daniel on 14/10/15.
 */
@Deprecated
public final class BitmapBlurScript {

    private static final float MAX_RADIUS_VALUE = 25f;

    private final Context context;
    private float radius = MAX_RADIUS_VALUE;
    private Bitmap bitmap = null;
    private boolean isTarget = false;
    private boolean mustRecycle = true;
    private ImageView imageView = null;

    public static BitmapBlurScript with(Context context) {
        return new BitmapBlurScript(context);
    }

    private BitmapBlurScript(Context context) {
        this.context = context;
    }

    public boolean canApplyTheBlur() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1);
    }

    public BitmapBlurScript setRadius(float radius) {
        this.radius = radius;
        return this;
    }

    public BitmapBlurScript setMustRecycle(boolean mustRecycle) {
        this.mustRecycle = mustRecycle;
        return this;
    }

    public void source(ImageView imageView) {
        this.source(imageView, false, null);
    }

    public void source(ImageView imageView, boolean isTarget) {
        this.source(imageView, isTarget, null);
    }

    public void source(ImageView imageView, OnBlurAppliedListener onBlurAppliedListener) {
        this.source(imageView, false, onBlurAppliedListener);
    }

    public void source(ImageView imageView, boolean isTarget, OnBlurAppliedListener onBlurAppliedListener) {
        mustRecycle = !isTarget;
        this.isTarget = isTarget;
        this.imageView = imageView;
        this.source(((BitmapDrawable) imageView.getDrawable()).getBitmap(),
                    onBlurAppliedListener);
    }

    public void source(Bitmap bitmap) {
        this.source(bitmap, null);
    }

    public void source(Bitmap bitmap, OnBlurAppliedListener onBlurAppliedListener) {
        this.bitmap = bitmap;
        if (!this.canApplyTheBlur()) {
            this.onPostExecuteTask(bitmap, onBlurAppliedListener);
            return;
        }
        BlurTask blurTask = new BlurTask(onBlurAppliedListener);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB)
            blurTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            blurTask.execute();
    }

    private void onPostExecuteTask(Bitmap bitmap, OnBlurAppliedListener onBlurAppliedListener) {
        if (isTarget && imageView != null)
            imageView.setImageBitmap(bitmap);
        if (onBlurAppliedListener != null)
            onBlurAppliedListener.onBlurApplied(bitmap);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private Bitmap processMultiBlur() {
        Bitmap result = bitmap;
        float currentRadius = radius;
        while (currentRadius > 0) {
            float nextRadius;
            if (currentRadius > MAX_RADIUS_VALUE)
                nextRadius = MAX_RADIUS_VALUE;
            else
                nextRadius = currentRadius;
            result = this.blurBitmap(result, nextRadius);
            currentRadius -= nextRadius;
        }
        return result;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private Bitmap blurBitmap(Bitmap bitmap, float radius) {
        //Let's create an empty bitmap with the same size of the bitmap we want to blur
        Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        //Instantiate a new Renderscript
        RenderScript renderScript = RenderScript.create(context.getApplicationContext());

        //Create an Intrinsic Blur Script using the Renderscript
        ScriptIntrinsicBlur scriptIntrinsicBlur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));

        //Create the Allocations (in/out) with the Renderscript and the in/out bitmaps
        Allocation allIn = Allocation.createFromBitmap(renderScript, bitmap);
        Allocation allOut = Allocation.createFromBitmap(renderScript, outBitmap);

        //Set the radius of the blur
        scriptIntrinsicBlur.setRadius(radius);

        //Perform the Renderscript
        scriptIntrinsicBlur.setInput(allIn);
        scriptIntrinsicBlur.forEach(allOut);

        //Copy the final bitmap created by the out Allocation to the outBitmap
        allOut.copyTo(outBitmap);

        //recycle the original bitmap
        if (mustRecycle && !bitmap.isRecycled())
            bitmap.recycle();

        //After finishing everything, we destroy the Renderscript.
        renderScript.destroy();

        return outBitmap;
    }

    private class BlurTask extends AsyncTask<Void, Bitmap, Bitmap> {
        private final OnBlurAppliedListener onBlurAppliedListener;
        private BlurTask(OnBlurAppliedListener onBlurAppliedListener) {
            this.onBlurAppliedListener = onBlurAppliedListener;
        }
        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap outBitmap = bitmap;
            try {
                outBitmap = processMultiBlur();
            } catch (Exception ignored) { }
            return outBitmap;
        }
        @Override
        protected void onProgressUpdate(Bitmap... values) {
            super.onProgressUpdate(values);
            if (isTarget)
                imageView.setImageBitmap(values[0]);
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            onPostExecuteTask(bitmap, onBlurAppliedListener);
        }
    }

    public interface OnBlurAppliedListener {
        void onBlurApplied(Bitmap blurBitmap);
    }

}
