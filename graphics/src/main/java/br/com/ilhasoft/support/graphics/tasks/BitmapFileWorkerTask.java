package br.com.ilhasoft.support.graphics.tasks;

import android.graphics.Bitmap;
import android.widget.ImageView;

import br.com.ilhasoft.support.graphics.BitmapDecoder;

/**
 * Created by johncordeiro on 03/09/15.
 */
public class BitmapFileWorkerTask extends BitmapWorkerTask<String> {

    public BitmapFileWorkerTask(ImageView imageView, int size) {
        super(imageView, size);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        pathName = params[0];
        return BitmapDecoder.decodeSampledBitmapFromFile(pathName, size, size);
    }
}
