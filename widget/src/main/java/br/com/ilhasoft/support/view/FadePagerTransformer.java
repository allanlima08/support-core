package br.com.ilhasoft.support.view;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by daniel on 30/11/16.
 */

public class FadePagerTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View page, float position) {
        page.setTranslationX(page.getWidth() * -position);

        if (position <= -1.0f || position >= 1.0f) {
            page.setAlpha(0.0f);
            page.setVisibility(View.GONE);
        } else if (position == 0.0f) {
            page.setAlpha(1.0f);
            page.setVisibility(View.VISIBLE);
        } else {
            // position is between -1.0f & 0.0f OR 0.0f & 1.0f
            page.setAlpha(1.0f - Math.abs(position));
            page.setVisibility(View.VISIBLE);
        }
    }

}
