package br.com.ilhasoft.support.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by johndalton on 24/01/15.
 */

public class UntouchableViewPager extends ViewPager {

    private boolean pagingEnabled = true;

    public UntouchableViewPager(Context context) {
        super(context);
    }

    public UntouchableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return pagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return pagingEnabled && super.onInterceptTouchEvent(event);
    }

    public boolean isPagingEnabled() {
        return pagingEnabled;
    }

    public void setPagingEnabled(boolean isPagingEnabled) {
        pagingEnabled = isPagingEnabled;
    }

}
