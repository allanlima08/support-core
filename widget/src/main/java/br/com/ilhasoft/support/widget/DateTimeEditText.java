package br.com.ilhasoft.support.widget;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.StyleableRes;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A {@link android.widget.EditText} that is used to enter dates and times using the native
 * dialogs and formatting what is shown.
 *
 * Created by daniel on 02/06/16.
 */

public class DateTimeEditText extends TextInputEditText implements View.OnClickListener {

    public static final long DEFAULT_TIME_IN_MILLIS = Long.MIN_VALUE;

    public static final int TYPE_DATE = 0;
    public static final int TYPE_TIME = 1;

    public static final String PATTERN_DATE = "MM/dd/yyyy";
    public static final String PATTERN_TIME = "hh:mm aa";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ TYPE_DATE, TYPE_TIME })
    public @interface DateTimeType { }

    @DateTimeType
    private int type;
    @Nullable
    private Date minDate;
    @Nullable
    private Date maxDate;
    private boolean is24Hour;
    private boolean isSafeChange;
    private Date defaultDateTime;
    @Nullable
    private DateTimeWatcher dateTimeWatcher;
    private SimpleDateFormat dateTimeFormat;
    @Nullable
    private OnClickListener clickListener;

    public DateTimeEditText(Context context) throws ParseException {
        super(context);
        this.init(context, null);
    }

    public DateTimeEditText(Context context, AttributeSet attrs) throws ParseException {
        super(context, attrs);
        this.init(context, attrs);
    }

    public DateTimeEditText(Context context, AttributeSet attrs, int defStyleAttr) throws ParseException {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs);
    }

    @SuppressWarnings({"WrongConstant", "ConstantConditions"})
    private void init(Context context, AttributeSet attrs) {
        super.setFocusable(false);
        super.setFocusableInTouchMode(false);
        super.setOnClickListener(this);
        super.addTextChangedListener(textWatcher);
        final Calendar calendar = Calendar.getInstance();

        minDate = null;
        maxDate = null;
        isSafeChange = false;
        defaultDateTime = calendar.getTime();
        dateTimeFormat = new SimpleDateFormat();
        if (attrs == null) {
            type = TYPE_DATE;
            dateTimeFormat.applyPattern(PATTERN_DATE);
            this.setDateTime(defaultDateTime);
            return;
        }

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DateTimeEditText, 0, 0);

        type = typedArray.getInt(R.styleable.DateTimeEditText_type, TYPE_DATE);
        is24Hour = typedArray.getBoolean(R.styleable.DateTimeEditText_is24Hour, false);

        final String pattern = typedArray.getString(R.styleable.DateTimeEditText_pattern);
        if (!TextUtils.isEmpty(pattern) && TextUtils.getTrimmedLength(pattern) > 0) {
            dateTimeFormat.applyPattern(pattern);
        } else {
            this.setPattern(PATTERN_DATE);
        }

        calendar.add(Calendar.DAY_OF_MONTH, typedArray.getInt(R.styleable.DateTimeEditText_minDateCount, Integer.MIN_VALUE));
        minDate = calendar.getTime();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.DAY_OF_MONTH, typedArray.getInt(R.styleable.DateTimeEditText_maxDateCount, Integer.MAX_VALUE));
        maxDate = calendar.getTime();

        minDate = this.parseAttrDate(typedArray, R.styleable.DateTimeEditText_minDate, minDate);
        maxDate = this.parseAttrDate(typedArray, R.styleable.DateTimeEditText_maxDate, maxDate);
        defaultDateTime = this.parseAttrDate(typedArray, R.styleable.DateTimeEditText_defaultDateTIme, defaultDateTime);

        if (defaultDateTime.before(this.minDate)) {
            defaultDateTime = this.minDate;
        } else if (defaultDateTime.after(this.maxDate)) {
            defaultDateTime = this.maxDate;
        }

        this.setDateTime(this.defaultDateTime);

        typedArray.recycle();
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        clickListener = onClickListener;
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        throw new UnsupportedOperationException("This EditText don't support this feature yet.");
    }

    @Override
    public void removeTextChangedListener(TextWatcher watcher) {
        throw new UnsupportedOperationException("This EditText don't support this feature yet.");
    }

    @Override
    public void setFocusable(boolean focusable) { }

    @Override
    public void setFocusableInTouchMode(boolean focusableInTouchMode) {
        throw new UnsupportedOperationException("This EditText is not focusable in touch mode.");
    }

    @Override
    public void onClick(View view) {
        if (clickListener != null) {
            clickListener.onClick(view);
        }

        final Calendar calendar = Calendar.getInstance();
        final Date date = this.getDate();
        if (date != null) {
            calendar.setTime(date);
        }

        if (type == TYPE_DATE) {
            this.showDatePicker(calendar);
        } else if (type == TYPE_TIME) {
            this.showTimePicker(calendar);
        } else {
            throw new IllegalStateException("There has been set a valid type.");
        }
    }

    private void showDatePicker(final Calendar calendar) {
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(),
                                                                       dateSetListener,
                                                                       calendar.get(Calendar.YEAR),
                                                                       calendar.get(Calendar.MONTH),
                                                                       calendar.get(Calendar.DAY_OF_MONTH));

        final DatePicker datePicker = datePickerDialog.getDatePicker();
        if (minDate != null) {
            datePicker.setMinDate(minDate.getTime());
        }
        if (maxDate != null) {
            datePicker.setMaxDate(maxDate.getTime());
        }
        datePickerDialog.show();
    }

    private void showTimePicker(final Calendar calendar) {
        new TimePickerDialog(this.getContext(), timeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
                             calendar.get(Calendar.MINUTE), is24Hour).show();
    }

    public void setDateTime(Date dateTime) {
        if (dateTime != null) {
            this.setDateTime(dateTimeFormat.format(dateTime));
        } else {
            this.setDateTime(dateTimeFormat.format(defaultDateTime));
        }
    }

    public void setDateTime(CharSequence dateTime) {
        isSafeChange = true;
        this.setText(dateTime);
        isSafeChange = false;
    }

    public String getPattern() {
        return dateTimeFormat.toPattern();
    }

    public void setPattern(@Nullable String pattern) {
        Date currentDate;
        try {
            currentDate = dateTimeFormat.parse(this.getDateString());
        } catch (ParseException ignored) {
            currentDate = null;
        }

        if (TextUtils.isEmpty(pattern) || TextUtils.getTrimmedLength(pattern) == 0) {
            if (type == TYPE_DATE) {
                pattern = PATTERN_DATE;
            } else {
                pattern = PATTERN_TIME;
            }
        }

        dateTimeFormat.applyPattern(pattern);
        if (currentDate != null) {
            this.setDateTime(currentDate);
        }
    }

    @DateTimeType
    public int getType() {
        return type;
    }

    public void setType(@DateTimeType int type) {
        this.type = type;
    }

    public Date getDefaultDateTime() {
        return defaultDateTime;
    }

    public void setDefaultDateTime(@Nullable Date defaultDateTime) {
        if (defaultDateTime != null) {
            this.defaultDateTime = defaultDateTime;
        } else {
            this.defaultDateTime = Calendar.getInstance().getTime();
        }
    }

    @Nullable
    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(@Nullable Date minDate) {
        this.minDate = minDate;
    }

    @Nullable
    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(@Nullable Date maxDate) {
        this.maxDate = maxDate;
    }

    public boolean is24Hour() {
        return is24Hour;
    }

    public void setIs24Hour(boolean is24Hour) {
        this.is24Hour = is24Hour;
    }

    public void setDateTimeWatcher(@Nullable DateTimeWatcher watcher) {
        dateTimeWatcher = watcher;
    }

    @Nullable
    public Calendar getCalendar() {
        return this.getCalendarInternal(this.getDateString());
    }

    @Nullable
    public Date getDate() {
        final Calendar calendar = getCalendarInternal(this.getDateString());
        return (calendar != null) ? calendar.getTime() : null;
    }

    public long getTimeInMillis() {
        final Calendar calendar = getCalendarInternal(getDateString());
        return (calendar != null) ? calendar.getTimeInMillis() : DateTimeEditText.DEFAULT_TIME_IN_MILLIS;
    }

    public String getDateString() {
        return this.getText().toString().trim();
    }

    @Nullable
    private Calendar getCalendarInternal(String dateTime) {
        try {
            final Date dateTmp = dateTimeFormat.parse(dateTime);
            final Calendar calendarCurrent = Calendar.getInstance();
            final Calendar calendar = Calendar.getInstance();

            calendar.setTime(dateTmp);
            if (type == TYPE_DATE) {
                calendar.set(Calendar.HOUR_OF_DAY, calendarCurrent.get(Calendar.HOUR_OF_DAY));
                calendar.set(Calendar.MINUTE, calendarCurrent.get(Calendar.MINUTE));
            } else if (type == TYPE_TIME) {
                calendar.set(Calendar.YEAR, calendarCurrent.get(Calendar.YEAR));
                calendar.set(Calendar.MONTH, calendarCurrent.get(Calendar.MONTH));
                calendar.set(Calendar.DAY_OF_MONTH, calendarCurrent.get(Calendar.DAY_OF_MONTH));
            }
            return calendar;
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("ConstantConditions")
    private Date parseAttrDate(TypedArray typedArray, @StyleableRes int styleableId, Date defaultDate) {
        try {
            final Calendar calendar = this.getCalendarInternal(typedArray.getString(styleableId));
            return calendar.getTime();
        } catch (Exception e) {
            return defaultDate;
        }
    }

    private void dispatchTryChangeDate(Date dateTime) {
        if (dateTimeWatcher == null) {
            this.setDateTime(dateTime);
        } else if (dateTimeWatcher.beforeDateChanged(dateTime)) {
            this.setDateTime(dateTime);
            dateTimeWatcher.onDateChanged(dateTime);
        }
    }

    private final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dispatchTryChangeDate(calendar.getTime());
        }
    };

    private final TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            dispatchTryChangeDate(calendar.getTime());
        }
    };

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (!isSafeChange) throw new IllegalStateException("You must set date using setDateTime method.");
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
        @Override
        public void afterTextChanged(Editable s) { }
    };

    public static class DateTimeWatcher {
        public boolean beforeDateChanged(Date dateTime) {
            return true;
        }
        public void onDateChanged(Date dateTime) { }
    }

}
