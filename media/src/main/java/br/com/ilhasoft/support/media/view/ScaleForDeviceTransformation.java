package br.com.ilhasoft.support.media.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.WindowManager;

import com.squareup.picasso.Transformation;

/**
 * Just an implementation of the {@link com.squareup.picasso.Transformation} interface
 * using {@link br.com.ilhasoft.support.graphics.transformations.ScaleForDeviceBitmapTransformation}.
 *
 * Created by daniel on 12/06/16.
 */
public class ScaleForDeviceTransformation implements Transformation {

    private final int maxWidth;
    private final int maxHeight;

    public ScaleForDeviceTransformation(Context context) {
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Point point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
        maxWidth = point.x * 2;
        maxHeight = point.y * 2;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final int sourceWidth = source.getWidth();
        final int sourceHeight = source.getHeight();
        // source is probably less than the maximum texture size
        if (sourceWidth < (maxWidth * 1.5) && sourceHeight < (maxHeight * 1.5)) {
            return source;
        }

        final int targetWidth;
        final int targetHeight;
        final double aspectRatio;

        if (source.getWidth() > source.getHeight()) {
            targetWidth = maxWidth;
            aspectRatio = (double) source.getHeight() / (double) source.getWidth();
            targetHeight = (int) Math.round(targetWidth * aspectRatio);
        } else {
            targetHeight = maxHeight;
            aspectRatio = (double) source.getWidth() / (double) source.getHeight();
            targetWidth = (int) Math.round(targetHeight * aspectRatio);
        }

        final Bitmap output = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
        if (output != source) {
            source.recycle();
        }

        return output;
    }

    @Override
    public String key() {
        return "scaled_for_device";
    }

}
