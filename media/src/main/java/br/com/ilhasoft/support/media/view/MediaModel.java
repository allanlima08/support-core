package br.com.ilhasoft.support.media.view;

import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

/**
 * Interface that a class must implement to be able to appear in {@link MediaViewFragment}.
 *
 * Created by johncordeiro on 20/08/15.
 */
public abstract class MediaModel implements Parcelable {

    public static final int NO_RESOURCE = -1;

    /**
     * @return The {@link Uri} to the image that must be shown.
     */
    public abstract Uri getUri();

    /**
     * Return a another image to be placed on top of the image.
     *
     * @return The drawable id or {@link MediaModel#NO_RESOURCE}
     */
    @DrawableRes
    public abstract int getOverlayDrawable();

    /**
     * @return The title id for this media.
     */
    @StringRes
    public abstract int getTitle();

    /**
     * @return Should return true if this MediaModel can be opened or false.
     */
    public abstract boolean hasOpenOption();

    /**
     * @return Default click listener for some kind of media
     */
    public void openMedia(Context context) {}

    /**
     * Return the icon to be placed on the action bar.
     *
     * @return The drawable id or {@link MediaModel#NO_RESOURCE}
     */
    @DrawableRes
    public abstract int getOpenDrawable();

}