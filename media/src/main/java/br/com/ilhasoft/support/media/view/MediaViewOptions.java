package br.com.ilhasoft.support.media.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by daniel on 25/08/16.
 */
public class MediaViewOptions {

    private final Bundle args;

    /**
     * Instantiate a {@link MediaViewOptions} with only a {@link MediaModel}.
     *
     * @param media The {@link MediaModel} that will appear in this {@link MediaViewOptions}.
     */
    public MediaViewOptions(final MediaModel media) {
        this(new ArrayList<MediaModel>(1) { {add(media);} });
    }

    /**
     * {@link MediaViewOptions(ArrayList, int)} method with parameter position
     * equal to 0.
     *
     * @param medias The {@link MediaModel} that will appear in this {@link MediaViewOptions}.
     */
    public MediaViewOptions(ArrayList<MediaModel> medias) {
        this(medias, 0);
    }

    /**
     * Instantiate a {@link MediaViewOptions} with a {@link ArrayList<MediaModel>} with the
     * option to choose the position where will start the display the medias.
     *
     * @param medias The {@link MediaModel} that will appear in this {@link MediaViewOptions}.
     * @param position The position of the item that will start the display of Medias.
     */
    public MediaViewOptions(ArrayList<MediaModel> medias, int position) {
        args = new Bundle();
        args.putBoolean(MediaViewActivity.EXTRA_AUTO_CLOSE_ENABLED, true);
        args.putParcelableArrayList(MediaViewFragment.ARG_MEDIAS, medias);

        final int size = medias.size();
        if (position >= size) args.putInt(MediaViewFragment.ARG_POSITION, size - 1);
        else if (position < 0 ) args.putInt(MediaViewFragment.ARG_POSITION, 0);
        else args.putInt(MediaViewFragment.ARG_POSITION, position);
    }

    public MediaViewOptions setAutoCloseEnabled(boolean isEnabled) {
        args.putBoolean(MediaViewActivity.EXTRA_AUTO_CLOSE_ENABLED, isEnabled);
        return this;
    }

    public MediaViewOptions setTitleEnabled(boolean titleEnabled) {
        args.putBoolean(MediaViewFragment.ARG_TITLE_ENABLED, titleEnabled);
        return this;
    }

    public MediaViewOptions setDefaultClickDisabled(boolean clickDisabled) {
        args.putBoolean(MediaViewFragment.ARG_DEFAULT_CLICK_DISABLED, clickDisabled);
        return this;
    }

    public MediaViewOptions setTitleColor(@ColorInt int titleColor) {
        args.putInt(MediaViewFragment.ARG_TITLE_COLOR, titleColor);
        return this;
    }

    public MediaViewOptions setTitleRes(@ColorRes int titleColorRes) {
        args.putInt(MediaViewFragment.ARG_TITLE_COLOR_RES, titleColorRes);
        return this;
    }

    public MediaViewOptions setToolbarColor(@ColorInt int toolbarColor) {
        args.putInt(MediaViewFragment.ARG_TOOLBAR_COLOR, toolbarColor);
        return this;
    }

    public MediaViewOptions setToolbarColorRes(@ColorRes int toolbarColorRes) {
        args.putInt(MediaViewFragment.ARG_TOOLBAR_COLOR_RES, toolbarColorRes);
        return this;
    }

    public MediaViewOptions setBackground(@ColorInt int backgroundColor) {
        args.putInt(MediaViewFragment.ARG_BACKGROUND, backgroundColor);
        return this;
    }

    public MediaViewOptions setBackgroundRes(@ColorRes int backgroundColorRes) {
        args.putInt(MediaViewFragment.ARG_BACKGROUND_RES, backgroundColorRes);
        return this;
    }

    public MediaViewOptions setBackButtonColor(@ColorInt int backButtonColor) {
        args.putInt(MediaViewFragment.ARG_BACK_BUTTON_COLOR, backButtonColor);
        return this;
    }

    public MediaViewOptions setBackButtonColorRes(@ColorRes int backButtonColorRes) {
        args.putInt(MediaViewFragment.ARG_BACK_BUTTON_COLOR_RES, backButtonColorRes);
        return this;
    }

    public MediaViewOptions setCloseButtonIcon(@DrawableRes int closeButtonIcon) {
        args.putInt(MediaViewFragment.ARG_CLOSE_BUTTON_ICON, closeButtonIcon);
        return this;
    }

    public MediaViewOptions setOnClickMediaListener(@Nullable OnClickMediaListener listener) {
        args.putSerializable(MediaViewFragment.ARG_ON_CLICK, listener);
        return this;
    }

    public MediaViewOptions setOnViewFinishedListener(@Nullable OnViewFinishedListener listener) {
        args.putSerializable(MediaViewFragment.ARG_ON_VIEW_FINISHED, listener);
        return this;
    }

    public MediaViewOptions setOnClickCloseListener(@Nullable OnClickCloseListener listener) {
        args.putSerializable(MediaViewFragment.ARG_ON_CLICK_CLOSE, listener);
        return this;
    }

    public MediaViewFragment createFragment() {
        return MediaViewFragment.newInstance(args);
    }

    public Intent createIntent(Context context) {
        return MediaViewActivity.createIntent(context, args);
    }

}
