package br.com.ilhasoft.support.media.view;

import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import br.com.ilhasoft.support.core.helpers.BundleHelper;
import br.com.ilhasoft.support.widget.TouchImageView;

/**
 * Internal fragment for use in {@link MediaViewFragment}.
 *
 * Created by johncordeiro on 04/09/15.
 */
public class MediaFragment extends Fragment {

    private static final String ARG_MEDIA = "media";

    private MediaModel media;
    private TouchImageView tivMedia;

    public static MediaFragment newInstance(MediaModel media) {
        final MediaFragment fragment = new MediaFragment();
        final Bundle args = new Bundle();
        args.putParcelable(ARG_MEDIA, media);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        media = BundleHelper.ensureArguments(this, ARG_MEDIA).getParcelable(ARG_MEDIA);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        tivMedia = new TouchImageView(getContext());
        return tivMedia;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Uri uri = media.getUri();
        if (Uri.EMPTY.equals(uri)) return;

        final List<Transformation> transformationList = new ArrayList<>();
        transformationList.add(new ScaleForDeviceTransformation(getContext()));
        if (media.getOverlayDrawable() != MediaModel.NO_RESOURCE) {
            transformationList.add(new OverlayTransformation(BitmapFactory.decodeResource(getResources(), media.getOverlayDrawable())));
        }

        view.setOnClickListener(clickListener);
        Picasso.with(getContext()).load(uri)
                .transform(transformationList)
                .into(tivMedia, onImageLoadedCallback);
    }

    private final View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final MediaViewFragment parentFragment = (MediaViewFragment) getParentFragment();
            final OnClickMediaListener listener = parentFragment.getOnClickMediaListener();
            if (listener != null) {
                listener.onClick(parentFragment, media);
            }
            if (!parentFragment.isDefaultClickDisabled()) {
                media.openMedia(getContext());
            }
        }
    };

    private final Callback onImageLoadedCallback = new Callback.EmptyCallback() {
        @Override
        public void onSuccess() {
            tivMedia.setZoom(tivMedia.getCurrentZoom());
        }
    };

}
