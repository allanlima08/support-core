package br.com.ilhasoft.support.media.view.models;

import android.net.Uri;
import android.os.Parcel;
import android.text.TextUtils;

import br.com.ilhasoft.support.media.view.MediaModel;

/**
 * A basic implementation of {@link MediaModel} that guarding a {@link Uri} to be shown in
 * {@link br.com.ilhasoft.support.media.view.MediaViewFragment}
 *
 * Created by daniel on 10/06/16.
 */
public abstract class UriMedia extends MediaModel {

    private Uri uri;

    public UriMedia(Uri uri) {
        this.setUri(uri);
    }

    public UriMedia(String url) {
        this.setUri(url);
    }

    protected UriMedia(Parcel in) {
        this.uri = in.readParcelable(Uri.class.getClassLoader());
    }

    @Override
    public Uri getUri() {
        return uri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(uri, flags);
    }

    public void setUri(Uri uri) {
        this.uri = (uri == null) ? Uri.EMPTY : uri;
    }

    public void setUri(String url) {
        uri = TextUtils.isEmpty(url) ? Uri.EMPTY : Uri.parse(url);
    }

}
