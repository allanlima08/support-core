package br.com.ilhasoft.support.media.view;

import java.io.Serializable;

/**
 * Created by daniel on 25/08/16.
 */
public interface OnViewFinishedListener extends Serializable {

    void onViewFinished(MediaViewFragment mediaViewFragment);

}
