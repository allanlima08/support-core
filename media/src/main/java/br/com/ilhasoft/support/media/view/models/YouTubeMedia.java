package br.com.ilhasoft.support.media.view.models;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import br.com.ilhasoft.support.media.R;
import br.com.ilhasoft.support.media.view.MediaModel;
import br.com.ilhasoft.support.media.YoutubeHelper;

/**
 * An implementation of {@link MediaModel} representing a YouTube url. It will show the thumbnail
 * of the video as an image. It will also be enabled the option to open this media, and show
 * a play button.
 *
 * Created by daniel on 10/06/16.
 */
public class YouTubeMedia extends MediaModel {

    private Uri thumbnailUri;
    private String youtubeUrl;

    /**
     * @param youtubeUrl The YouTube url that this {@link MediaModel} will represent.
     */
    public YouTubeMedia(String youtubeUrl) {
        this.setYoutubeUrl(youtubeUrl);
    }

    protected YouTubeMedia(Parcel in) {
        this.thumbnailUri = in.readParcelable(Uri.class.getClassLoader());
        this.youtubeUrl = in.readString();
    }

    @DrawableRes
    @Override
    public int getOverlayDrawable() {
        return R.drawable.img_button_play;
    }

    @StringRes
    @Override
    public int getTitle() {
        return R.string.youtube;
    }

    @Override
    public boolean hasOpenOption() {
        return true;
    }

    @Override
    public void openMedia(Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeUrl)));
    }

    @Override
    public Uri getUri() {
        return thumbnailUri;
    }

    @Override
    public int getOpenDrawable() {
        return R.drawable.ic_open_in_browser_white_24dp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.thumbnailUri, flags);
        dest.writeString(this.youtubeUrl);
    }

    /**
     * @return The YouTube url that this {@link MediaModel} represents.
     */
    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    /**
     * @param youtubeUrl The YouTube url that this {@link MediaModel} will represent.
     */
    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
        setThumbnailUri(youtubeUrl);
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeUrl = YoutubeHelper.getVideoUrlFromId(youtubeId);
        setThumbnailUri(youtubeUrl);
    }

    private void setThumbnailUri(String youtubeUrl) {
        if (TextUtils.isEmpty(youtubeUrl)) {
            thumbnailUri = Uri.EMPTY;
        } else {
            thumbnailUri = Uri.parse(YoutubeHelper.getThumbnailUrlFromUrl(youtubeUrl, YoutubeHelper.THUMBNAIL_HIGH));
        }
    }

    public static final Creator<YouTubeMedia> CREATOR = new Creator<YouTubeMedia>() {
        @Override
        public YouTubeMedia createFromParcel(Parcel source) {
            return new YouTubeMedia(source);
        }
        @Override
        public YouTubeMedia[] newArray(int size) {
            return new YouTubeMedia[size];
        }
    };

}
