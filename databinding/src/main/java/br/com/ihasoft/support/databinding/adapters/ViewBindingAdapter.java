package br.com.ihasoft.support.databinding.adapters;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;

/**
 * Created by john-mac on 5/28/16.
 */
@SuppressWarnings("unused")
public final class ViewBindingAdapter {

    private ViewBindingAdapter() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @BindingAdapter({"android:background"})
    public static void setBackground(View view, String color) {
        if (TextUtils.isEmpty(color)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(null);
            } else {
                view.setBackground(null);
            }
        } else {
            view.setBackgroundColor(Color.parseColor(color));
        }
    }

    @BindingAdapter({"visible"})
    public static void setVisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

}
