package br.com.ihasoft.support.databinding.adapters;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by john-mac on 5/28/16.
 */
@SuppressWarnings("unused")
public final class ImageViewBindingAdapter {

    private ImageViewBindingAdapter() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @BindingAdapter({"android:src"})
    public static void setImageRes(ImageView imageView, @DrawableRes int imageRes) {
        imageView.setImageResource(imageRes);
    }

    @BindingAdapter(value = {"imageUrl", "placeholderRes", "errorRes", "centerCrop"}, requireAll = false)
    public static void loadImage(ImageView imageView, @Nullable String url,
                                 @DrawableRes int placeholderRes, @DrawableRes int errorRes,
                                 boolean centerCrop) {
        if (TextUtils.isEmpty(url)) return;

        final Drawable placeholder = placeholderRes != 0 ? ContextCompat.getDrawable(imageView.getContext(), placeholderRes) : null;
        final Drawable error = errorRes != 0 ? ContextCompat.getDrawable(imageView.getContext(), errorRes) : null;

        ImageViewBindingAdapter.loadImage(imageView, url, placeholder, error, centerCrop);
    }

    @BindingAdapter(value = {"imageUrl", "placeholder", "error", "centerCrop"}, requireAll = false)
    public static void loadImage(ImageView imageView, @Nullable String url,
                                 @Nullable Drawable placeholder, @Nullable Drawable error,
                                 boolean centerCrop) {
        final RequestCreator requestCreator = Picasso.with(imageView.getContext()).load(url);

        if (centerCrop) requestCreator.fit().centerCrop();
        if (error != null) requestCreator.error(error);
        if (placeholder != null) requestCreator.placeholder(placeholder);

        requestCreator.into(imageView);
    }

    @BindingAdapter({"colorFilter"})
    public static void setColorFilter(ImageView imageView, String colorHex) {
        if (TextUtils.isEmpty(colorHex)) {
            imageView.clearColorFilter();
        } else {
            ImageViewBindingAdapter.setColorFilter(imageView, Color.parseColor(colorHex));
        }
    }

    @BindingAdapter({"colorFilter"})
    public static void setColorFilter(ImageView imageView, @ColorInt int colorInt) {
        // TODO: Decide between ColorInt or ColorRes
        imageView.setColorFilter(new PorterDuffColorFilter(colorInt, PorterDuff.Mode.SRC_ATOP));
    }

}
