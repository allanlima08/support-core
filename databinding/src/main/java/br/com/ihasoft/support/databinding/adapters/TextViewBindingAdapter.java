package br.com.ihasoft.support.databinding.adapters;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;

/**
 * Created by john-mac on 5/28/16.
 */
@SuppressWarnings("unused")
public final class TextViewBindingAdapter {

    private TextViewBindingAdapter() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @BindingAdapter({"disableIfEmpty"})
    public static void setDisableIfEmpty(TextView view, boolean disableIfEmpty) {
        // TODO: Consider implementing this function dynamically with TextWatcher
        if (disableIfEmpty) view.setVisibility(view.getText().length() > 0 ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"android:textColor"})
    public static void setTextColor(TextView textView, String color) {
        if (!TextUtils.isEmpty(color)) textView.setTextColor(Color.parseColor(color));
    }

    @BindingAdapter({"textColorRes"})
    public static void setTextColorRes(TextView textView, @ColorRes int colorRes) {
        textView.setTextColor(ContextCompat.getColor(textView.getContext(), colorRes));
    }

    @BindingAdapter({"mask"})
    public static void bindMask(EditText view, String mask) {
        // TODO: Use android.databinding.adapters.ListenerUtil
        MaskEditTextChangedListener listener = new MaskEditTextChangedListener(mask, view);
        view.addTextChangedListener(listener);
    }

}

