package br.com.ilhasoft.support.recyclerview.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 04/10/15.
 */
public abstract class FilterableRecyclerAdapter<E, VH extends RecyclerView.ViewHolder>
        extends OnDemandRecyclerAdapter<E, VH> implements Filterable {

    private ListFilter listFilter;
    private boolean isFiltered = false;
    private boolean originalEnableOnDemand = true;
    private final Object lock = new Object();
    private final List<E> originalObjects = new ArrayList<>();

    public FilterableRecyclerAdapter() {
        super();
    }

    public FilterableRecyclerAdapter(OnDemandListener onDemandListener) {
        super(onDemandListener);
    }

    public FilterableRecyclerAdapter(int capacity) {
        super(capacity);
    }

    public FilterableRecyclerAdapter(List<E> list) {
        super(list);
    }

    public FilterableRecyclerAdapter(Collection<? extends E> collection) {
        super(collection);
    }

    @Override
    public void setEnableOnDemand(boolean enableOnDemand) {
        super.setEnableOnDemand(enableOnDemand);
        this.originalEnableOnDemand = enableOnDemand;
    }

    @Override
    public void add(int location, E object) {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.add(location, object);
            else
                super.add(location, object);
        }
    }

    @Override
    public boolean add(E object) {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.add(object);
            else
                super.add(object);
        }
        return true;
    }

    @Override
    public boolean addAll(int location, Collection<? extends E> collection) {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.addAll(location, collection);
            else
                super.addAll(location, collection);
        }
        return true;
    }

    @Override
    public boolean addAllObjects(int location, Collection<? extends E> collection) {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.addAll(location, collection);
            else
                super.addAllObjects(location, collection);
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.addAll(collection);
            else
                super.addAll(collection);
        }
        return true;
    }

    @Override
    public void clear() {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.clear();
            else
                super.clear();
        }
    }

    @Override
    public void clearObjects() {
        synchronized (lock) {
            if (isFiltered)
                originalObjects.clear();
            else
                super.clearObjects();
        }
    }

    @Override
    public E remove(int location) {
        E object;
        synchronized (lock) {
            if (isFiltered)
                object = originalObjects.remove(location);
            else
                object = super.remove(location);
        }
        return object;
    }

    @Override
    public boolean remove(Object object) {
        boolean result;
        synchronized (lock) {
            if (isFiltered)
                result = originalObjects.remove(object);
            else
                result = super.remove(object);
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean result;
        synchronized (lock) {
            if (isFiltered)
                result = originalObjects.removeAll(collection);
            else
                result = super.removeAll(collection);
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean result;
        synchronized (lock) {
            if (isFiltered)
                result = originalObjects.retainAll(collection);
            else
                result = super.retainAll(collection);
        }
        return result;
    }

    @Override
    public E set(int location, E object) {
        E oldObject;
        synchronized (lock) {
            if (isFiltered)
                oldObject = originalObjects.set(location, object);
            else
                oldObject = super.set(location, object);
        }
        return oldObject;
    }

    @Override
    public void sort(Comparator<? super E> comparator) {
        synchronized (lock) {
            if (isFiltered)
                Collections.sort(originalObjects, comparator);
            else
                super.sort(comparator);
        }
    }

    @Override
    public Filter getFilter() {
        if (listFilter == null) {
            listFilter = new ListFilter();
        }
        return listFilter;
    }

    protected boolean mustAddToTheResult(final String filterPattern, final E value) {
        return this.mustAddToTheResult(filterPattern, value.toString().toLowerCase());
    }

    protected boolean mustAddToTheResult(final String filterPattern, final String valueText) {
        // First match against the whole, non-splitted value
        if (valueText.startsWith(filterPattern)) {
            return true;
        } else {
            final String[] words = valueText.split(" ");

            // Start at index 0, in case valueText starts with space(s)
            for (String word : words) {
                if (word.startsWith(filterPattern)) {
                    return true;
                }
            }
        }
        return false;
    }

    private class ListFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults results = new FilterResults();
            final ArrayList<E> values = new ArrayList<>();

            if (!isFiltered && originalObjects.size() != size()) {
                synchronized (lock) {
                    originalObjects.clear();
                    originalObjects.addAll(getList());
                }
            }

            if (TextUtils.isEmpty(constraint)) {
                values.addAll(originalObjects);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (E value : originalObjects) {
                    if (mustAddToTheResult(filterPattern, value))
                        values.add(value);
                }
            }

            results.values = values;
            results.count = values.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            isFiltered = (originalObjects.size() != ((List) results.values).size());
            FilterableRecyclerAdapter.super.setEnableOnDemand(!isFiltered && originalEnableOnDemand);
            //noinspection unchecked
            swapObjects((ArrayList<E>) results.values);
        }
    }

}
