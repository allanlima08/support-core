package br.com.ilhasoft.support.recyclerview.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by daniel on 16/06/16.
 */
public interface OnCreateViewHolder<E, VH extends ViewHolder<E>> {

    VH onCreateViewHolder(LayoutInflater layoutInflater, ViewGroup parent, int viewType);

}
