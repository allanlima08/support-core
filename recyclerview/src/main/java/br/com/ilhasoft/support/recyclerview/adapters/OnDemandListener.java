package br.com.ilhasoft.support.recyclerview.adapters;

/**
 * Created by daniel on 30/03/16.
 */
public interface OnDemandListener {

    void onLoadMore();

}
