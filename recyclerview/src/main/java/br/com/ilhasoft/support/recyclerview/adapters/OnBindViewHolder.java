package br.com.ilhasoft.support.recyclerview.adapters;

/**
 * Created by daniel on 16/06/16.
 */
public interface OnBindViewHolder<E, VH extends ViewHolder<E>> {

    void onBindViewHolder(VH holder, E object, int position);

}