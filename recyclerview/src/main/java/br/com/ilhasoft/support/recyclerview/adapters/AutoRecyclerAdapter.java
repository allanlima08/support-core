package br.com.ilhasoft.support.recyclerview.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.Collection;
import java.util.List;

/**
 * Created by daniel on 13/06/16.
 */
public class AutoRecyclerAdapter<E, VH extends ViewHolder<E>>
        extends FilterableRecyclerAdapter<E, VH> {

    private final AutoRecyclerAdapterDelegate<E, VH> delegate;

    public AutoRecyclerAdapter(OnCreateViewHolder<E, VH> onCreateViewHolder) {
        super();
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    public AutoRecyclerAdapter(OnCreateViewHolder<E, VH> onCreateViewHolder,
                               @Nullable OnDemandListener onDemandListener) {
        super(onDemandListener);
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    public AutoRecyclerAdapter(int capacity, OnCreateViewHolder<E, VH> onCreateViewHolder) {
        super(capacity);
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    public AutoRecyclerAdapter(List<E> list, OnCreateViewHolder<E, VH> onCreateViewHolder) {
        super(list);
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    public AutoRecyclerAdapter(Collection<? extends E> collection,
                               OnCreateViewHolder<E, VH> onCreateViewHolder) {
        super(collection);
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        delegate.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegate.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        delegate.onBindViewHolder(holder, this.get(position), position);
    }

    @Nullable
    public OnBindViewHolder<E, VH> getOnBindViewHolder() {
        return delegate.getOnBindViewHolder();
    }

    public void setOnBindViewHolder(@Nullable OnBindViewHolder<E, VH> onBindViewHolder) {
        delegate.setOnBindViewHolder(onBindViewHolder);
    }

}
