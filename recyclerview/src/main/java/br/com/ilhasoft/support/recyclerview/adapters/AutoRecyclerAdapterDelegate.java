package br.com.ilhasoft.support.recyclerview.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by daniel on 16/06/16.
 */
public class AutoRecyclerAdapterDelegate<E, VH extends ViewHolder<E>> {

    private LayoutInflater layoutInflater;
    private OnBindViewHolder<E, VH> onBindViewHolder;
    private final OnCreateViewHolder<E, VH> onCreateViewHolder;

    public AutoRecyclerAdapterDelegate(OnCreateViewHolder<E, VH> onCreateViewHolder) {
        this.onCreateViewHolder = onCreateViewHolder;
    }

    public AutoRecyclerAdapterDelegate(OnCreateViewHolder<E, VH> onCreateViewHolder,
                                       @Nullable OnBindViewHolder<E, VH> onBindViewHolder) {
        this.onBindViewHolder = onBindViewHolder;
        this.onCreateViewHolder = onCreateViewHolder;
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        layoutInflater = LayoutInflater.from(recyclerView.getContext());
    }

    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateViewHolder.onCreateViewHolder(layoutInflater, parent, viewType);
    }

    public void onBindViewHolder(VH holder, E object, int position) {
        holder.bind(object);
        if (onBindViewHolder != null) {
            onBindViewHolder.onBindViewHolder(holder, object, position);
        }
    }

    @Nullable
    public OnBindViewHolder<E, VH> getOnBindViewHolder() {
        return onBindViewHolder;
    }

    public void setOnBindViewHolder(@Nullable OnBindViewHolder<E, VH> onBindViewHolder) {
        this.onBindViewHolder = onBindViewHolder;
    }

}
