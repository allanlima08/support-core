package br.com.ilhasoft.support.recyclerview.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by daniel on 30/03/16.
 */
public class RecyclerAdapterWrapper<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private RecyclerView parent;
    private RecyclerView.Adapter<VH> wrappedAdapter;

    public RecyclerAdapterWrapper(RecyclerView.Adapter<VH> wrappedAdapter) {
        wrappedAdapter.registerAdapterDataObserver(adapterDataObserver);
        this.wrappedAdapter = wrappedAdapter;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return wrappedAdapter.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        wrappedAdapter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemViewType(int position) {
        return wrappedAdapter.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
        wrappedAdapter.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return wrappedAdapter.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return wrappedAdapter.getItemCount();
    }

    @Override
    public void onViewRecycled(VH holder) {
        wrappedAdapter.onViewRecycled(holder);
    }

    @Override
    public boolean onFailedToRecycleView(VH holder) {
        return super.onFailedToRecycleView(holder) && wrappedAdapter.onFailedToRecycleView(holder);
    }

    @Override
    public void onViewAttachedToWindow(VH holder) {
        super.onViewAttachedToWindow(holder);
        wrappedAdapter.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(VH holder) {
        super.onViewDetachedFromWindow(holder);
        wrappedAdapter.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        parent = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
        wrappedAdapter.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        parent = null;
        super.onDetachedFromRecyclerView(recyclerView);
        wrappedAdapter.onDetachedFromRecyclerView(recyclerView);
    }

    public final RecyclerView.Adapter<VH> getWrappedAdapter() {
        return wrappedAdapter;
    }

    public void setWrappedAdapter(RecyclerView.Adapter<VH> wrappedAdapter) {
        if (this.wrappedAdapter.equals(wrappedAdapter)) {
            notifyDataSetChanged();
            return;
        } else if (parent != null) {
            this.wrappedAdapter.onDetachedFromRecyclerView(parent);
            wrappedAdapter.onAttachedToRecyclerView(parent);
        }

        this.wrappedAdapter.unregisterAdapterDataObserver(adapterDataObserver);
        wrappedAdapter.registerAdapterDataObserver(adapterDataObserver);

        this.wrappedAdapter = wrappedAdapter;
        notifyDataSetChanged();
    }

    protected void notifyDataSetChangedWrapper() {
        notifyDataSetChanged();
    }

    protected void notifyItemRangeChangedWrapper(int positionStart, int itemCount) {
        notifyItemRangeChanged(positionStart, itemCount);
    }

    protected void notifyItemRangeChangedWrapper(int positionStart, int itemCount, Object payload) {
        notifyItemRangeChanged(positionStart, itemCount, payload);
    }

    protected void notifyItemRangeInsertedWrapper(int positionStart, int itemCount) {
        notifyItemRangeInserted(positionStart, itemCount);
    }

    protected void notifyItemRangeRemovedWrapper(int positionStart, int itemCount) {
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    protected void notifyItemRangeChangedWrapper(int fromPosition, int toPosition, int itemCount) {
        notifyItemRangeChanged(fromPosition, fromPosition + itemCount);
    }

    private final RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            notifyDataSetChangedWrapper();
        }
        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            notifyItemRangeChangedWrapper(positionStart, itemCount);
        }
        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            super.onItemRangeChanged(positionStart, itemCount, payload);
            notifyItemRangeChangedWrapper(positionStart, itemCount, payload);
        }
        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            notifyItemRangeInsertedWrapper(positionStart, itemCount);
        }
        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            notifyItemRangeRemovedWrapper(positionStart, itemCount);
        }
        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            notifyItemRangeChangedWrapper(fromPosition, toPosition, itemCount);
        }
    };

}
