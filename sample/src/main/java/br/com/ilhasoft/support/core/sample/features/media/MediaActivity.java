package br.com.ilhasoft.support.core.sample.features.media;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.util.List;

import br.com.ilhasoft.support.core.helpers.IoHelper;
import br.com.ilhasoft.support.core.sample.R;
import br.com.ilhasoft.support.core.sample.databinding.ActivityMediaBinding;
import br.com.ilhasoft.support.graphics.tasks.BitmapUriWorkerTask;
import br.com.ilhasoft.support.media.MediaSelectorDelegate;
import br.com.ilhasoft.support.media.YoutubeHelper;
import br.com.ilhasoft.support.rxgraphics.FileCompressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MediaActivity extends AppCompatActivity {

    private static final String TAG = "MediaActivity";

    private ActivityMediaBinding binding;
    private MediaSelectorDelegate delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_media);

        delegate = new MediaSelectorDelegate(this, "br.com.ilhasoft.support.core.sample.provider")
                .setOnLoadFileListener(onLoadImageListener)
                .setOnLoadMultipleFilesListener(onLoadMediaSampleListener)
                .setOnLoadGalleryMultipleImagesListener(onLoadMediaSampleListener)
                .setOnLoadGalleryImageListener(onLoadImageListener)
                .setOnLoadImageListener(onLoadImageListener)
                .setOnLoadVideoListener(onLoadVideoListener)
                .setOnLoadAudioListener(onLoadAudioListener)
                .setOnPickYoutubeVideoListener(onPickYoutubeVideoListener)
                .setOnErrorLoadingMediaListener(onErrorLoadingMediaListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        delegate.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        delegate.onRequestPermissionResult(this, requestCode, grantResults);
    }

    public void getPicture(View view) {
        delegate.selectMedia(this, MediaSelectorDelegate.FILE);
    }

    public void getMedia(View view) {
        delegate.selectMedia(this);
    }

    private MediaSelectorDelegate.OnLoadMediaListener onLoadImageListener = new MediaSelectorDelegate.OnLoadMediaListener() {
        @Override
        public void onLoadMedia(Uri uri) {
            BitmapUriWorkerTask bitmapUriWorkerTask = new BitmapUriWorkerTask(MediaActivity.this, binding.image, 200);
            bitmapUriWorkerTask.execute(uri);
        }
    };

    private YoutubeHelper.OnPickYoutubeVideoListener onPickYoutubeVideoListener = new YoutubeHelper.OnPickYoutubeVideoListener() {
        @Override
        public void onPickYoutubeVideo(String videoId, String videoUrl) {
            Snackbar.make(binding.getRoot(), "Video Id: " + videoId, Snackbar.LENGTH_LONG).show();
        }
    };

    private MediaSelectorDelegate.OnLoadMultipleMediaListener onLoadMediaSampleListener = new MediaSelectorDelegate.OnLoadMultipleMediaListener() {
        @Override
        public void onLoadMultipleMedia(List<Uri> uriList) {
            Snackbar.make(binding.getRoot(), uriList.toString(), Snackbar.LENGTH_LONG).show();
        }
    };

    private MediaSelectorDelegate.OnLoadAudioListener onLoadAudioListener = new MediaSelectorDelegate.OnLoadAudioListener() {
        @Override
        public void onLoadAudio(Uri uri, int duration) {
            Snackbar.make(binding.getRoot(), "Audio: " + uri + "], duration = [" + duration + "]"
                    , Snackbar.LENGTH_LONG).show();
        }
    };

    private MediaSelectorDelegate.OnLoadMediaListener onLoadVideoListener = new MediaSelectorDelegate.OnLoadMediaListener() {
        @Override
        public void onLoadMedia(final Uri uri) {
            try {
                FileCompressor.compressVideo(new File(IoHelper.getFilePathForUri(MediaActivity.this, uri)))
                        .subscribeOn(Schedulers.newThread())
                        .subscribe(new Consumer<File>() {
                            @Override
                            public void accept(@io.reactivex.annotations.NonNull File file) throws Exception {
                                Snackbar.make(binding.getRoot(), file.getAbsolutePath(), Snackbar.LENGTH_LONG).show();
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                                Log.e(TAG, "call: ", throwable);
                            }
                        });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    private MediaSelectorDelegate.OnErrorLoadingMediaListener onErrorLoadingMediaListener = new MediaSelectorDelegate.OnErrorLoadingMediaListener() {
        @Override
        public void onErrorLoadingMedia(Exception exception, int errorMessageId) {
            Snackbar.make(binding.getRoot(), getString(errorMessageId), Snackbar.LENGTH_LONG).show();
        }
    };

}
