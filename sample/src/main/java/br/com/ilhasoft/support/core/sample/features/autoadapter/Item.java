package br.com.ilhasoft.support.core.sample.features.autoadapter;

/**
 * Created by daniel on 13/06/16.
 */
public class Item {

    private String text;

    public Item() { }

    public Item(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
