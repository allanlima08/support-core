package br.com.ilhasoft.support.core.sample.features.autoadapter;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableInt;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.ilhasoft.support.core.sample.R;
import br.com.ilhasoft.support.core.sample.databinding.ActivityAutoAdapterBinding;
import br.com.ilhasoft.support.core.text.EmptyTextWatcher;
import br.com.ilhasoft.support.recyclerview.adapters.AutoRecyclerAdapter;
import br.com.ilhasoft.support.recyclerview.adapters.OnDemandListener;
import br.com.ilhasoft.support.recyclerview.adapters.OnCreateViewHolder;

/**
 * Created by daniel on 13/06/16.
 */
public class AutoAdapterActivity extends AppCompatActivity
        implements OnDemandListener, OnCreateViewHolder<Item,ItemViewHolder> {

    private ObservableInt count;
    private ActivityAutoAdapterBinding binding;
    private AutoRecyclerAdapter<Item, ItemViewHolder> adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        count = new ObservableInt(0);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auto_adapter);
        binding.setCount(count);

        adapter = new AutoRecyclerAdapter<>(this);
        binding.rvList.setAdapter(adapter);
        binding.etFilter.addTextChangedListener(emptyTextWatcher);

        adapter.registerAdapterDataObserver(dataObserver);
        adapter.setOnDemandListener(this);
        adapter.addAll(this.generateItem());
    }

    @Override
    protected void onDestroy() {
        adapter.unregisterAdapterDataObserver(dataObserver);
        super.onDestroy();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(LayoutInflater layoutInflater, ViewGroup parent,
                                             int viewType) {
        return new ItemViewHolder(layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false));
    }

    @Override
    public void onLoadMore() {
        adapter.addAll(this.generateItem());
    }

    private List<Item> generateItem() {
        final int count = new Random().nextInt(20);
        final List<Item> items = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            items.add(new Item(this.nextRandomString()));
        }
        return items;
    }

    private String nextRandomString() {
        final CharSequence characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        final StringBuilder string = new StringBuilder();
        Random random = new Random();
        while (string.length() < 8) {
            string.append(characters.charAt(random.nextInt(characters.length())));
        }
        return string.toString();
    }

    private final EmptyTextWatcher emptyTextWatcher = new EmptyTextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            super.onTextChanged(s, start, before, count);
            adapter.getFilter().filter(s);
        }
    };

    private final RecyclerView.AdapterDataObserver dataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            count.set(adapter.getItemCount());
        }
        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            count.set(adapter.getItemCount());
        }
        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            count.set(adapter.getItemCount());
        }
    };

}
