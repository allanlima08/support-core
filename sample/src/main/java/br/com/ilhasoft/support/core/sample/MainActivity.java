package br.com.ilhasoft.support.core.sample;

import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.concurrent.TimeUnit;

import br.com.ilhasoft.support.core.app.IndeterminateProgressDialog;
import br.com.ilhasoft.support.core.sample.databinding.ActivityMainBinding;
import br.com.ilhasoft.support.core.sample.features.autoadapter.AutoAdapterActivity;
import br.com.ilhasoft.support.core.sample.features.headeradapter.HeaderAdapterActivity;
import br.com.ilhasoft.support.core.sample.features.media.MediaActivity;
import br.com.ilhasoft.support.core.sample.features.mediaview.MediaViewProxyActivity;
import br.com.ilhasoft.support.core.sample.features.widgets.WidgetsActivity;
import br.com.ilhasoft.support.core.sample.models.ItemSample;
import br.com.ilhasoft.support.graphics.drawable.badge.BadgeOptionMenuDrawable;

public class MainActivity extends AppCompatActivity {

    private int count;
    private ActivityMainBinding binding;
    private BadgeOptionMenuDrawable badgeOptionMenuDrawable;

    private final ItemSample [] samples = {new ItemSample("Widgets", WidgetsActivity.class),
                                           new ItemSample("Media", MediaActivity.class),
                                           new ItemSample("Media view", MediaViewProxyActivity.class),
                                           new ItemSample("Header adapter", HeaderAdapterActivity.class),
                                           new ItemSample("Auto adapter with filter", AutoAdapterActivity.class)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupList();
        showLoading();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ilhasoft_core_menu_main, menu);
        menu.findItem(R.id.mn_remove).setIcon(VectorDrawableCompat.create(getResources(), R.drawable.ic_remove_circle_outline_white_24dp, getTheme()));

        final Drawable addIcon = VectorDrawableCompat.create(getResources(), R.drawable.ic_add_circle_outline_white_24dp, getTheme());
        badgeOptionMenuDrawable = BadgeOptionMenuDrawable.createWithDrawable(this, menu.findItem(R.id.mn_add), addIcon);
        badgeOptionMenuDrawable.setBadgeNumber(count);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_add:
                if (count > 98) return true;
                badgeOptionMenuDrawable.setBadgeNumber(++count);
                return true;
            case R.id.mn_remove:
                if (count == 0) return true;
                badgeOptionMenuDrawable.setBadgeNumber(--count);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupList() {
        ArrayAdapter<ItemSample> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, samples);
        binding.list.setAdapter(arrayAdapter);
        binding.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(MainActivity.this, samples[position].mClass));
            }
        });
    }

    private void showLoading() {
        final Dialog dialog = IndeterminateProgressDialog.show(this, "Wait title", "Wait message...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, TimeUnit.SECONDS.toMillis(2));
    }

}
