package br.com.ilhasoft.support.core.sample.features.mediaview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

import br.com.ilhasoft.support.media.view.MediaModel;
import br.com.ilhasoft.support.media.view.MediaViewFragment;
import br.com.ilhasoft.support.media.view.MediaViewOptions;
import br.com.ilhasoft.support.media.view.OnClickCloseListener;
import br.com.ilhasoft.support.media.view.OnClickMediaListener;
import br.com.ilhasoft.support.media.view.OnViewFinishedListener;
import br.com.ilhasoft.support.media.view.models.ImageMedia;
import br.com.ilhasoft.support.media.view.models.VideoMedia;
import br.com.ilhasoft.support.media.view.models.YouTubeMedia;

/**
 * Created by daniel on 10/06/16.
 */
public class MediaViewProxyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) return;

        final ArrayList<MediaModel> mediaModelList = new ArrayList<>();
        mediaModelList.add(new ImageMedia("http://4.bp.blogspot.com/-M3JBxZu90Qc/VWN5euhOa-I/AAAAAAAADro/Np9MgYism6U/s1600/Anime_Key_Visuals.png"));
        mediaModelList.add(new VideoMedia("https://ilhacloud-dev.s3.amazonaws.com/clickcondo/files/068da4615b049e88cc8fa703c163ca3f_VIDEO_20161205_175619_435095857.mp4"
        , "https://ilhacloud-dev.s3.amazonaws.com/clickcondo/files/c83b5950a01e73dafa327779dced93bf_JPEG_20161205_175619_1472840156.jpg"));
        mediaModelList.add(new YouTubeMedia("https://www.youtube.com/watch?v=wWs2dqKRgdI"));
        mediaModelList.add(new ImageMedia("http://s2.glbimg.com/Ahf5eBvUF6Qx_p8w7yOYG8D2Kp8=/0x600/s.glbimg.com/po/tt2/f/original/2015/08/31/gnulinux-logo.png"));
        mediaModelList.add(new ImageMedia("http://d139lereoqc85y.cloudfront.net/wp-content/uploads/2014/07/dota-2-official.jpg"));
        mediaModelList.add(new YouTubeMedia("https://www.youtube.com/watch?v=uo35R9zQsAI"));

        final Listener listener = new Listener();
        startActivity(new MediaViewOptions(mediaModelList)
                .setOnClickCloseListener(listener)
                .setOnViewFinishedListener(listener)
                .createIntent(this));
        finish();
    }

    public static final class Listener implements OnClickMediaListener,
            OnViewFinishedListener, OnClickCloseListener {
        private String TAG = "=========";
        public Listener() { }
        @Override
        public void onClick(MediaViewFragment mediaViewFragment) {
            Log.d(TAG, "onClick: " + mediaViewFragment);
        }
        @Override
        public void onClick(MediaViewFragment mediaViewFragment, MediaModel mediaModel) {
            final Uri uri;
            if (mediaModel instanceof YouTubeMedia) {
                uri = Uri.parse(((YouTubeMedia) mediaModel).getYoutubeUrl());
            } else {
                uri = mediaModel.getUri();
            }
            mediaViewFragment.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
        @Override
        public void onViewFinished(MediaViewFragment mediaViewFragment) {
            Log.d(TAG, "onViewFinished: " + mediaViewFragment);
        }
    }

}
