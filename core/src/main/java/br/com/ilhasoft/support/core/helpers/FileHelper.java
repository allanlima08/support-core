package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by danielsan on 8/20/15.
 */
public final class FileHelper {

    private static final String TAG = "FileHelper";

    private static final String DOCUMENT_URL = "http://docs.google.com/gview?embedded=true&url=%1$s";

    private FileHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @Nullable
    public static File generateTempFile(Context context, String prefix, String suffix) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault());
        final String fileName = String.format("%s_%s_", prefix, simpleDateFormat.format(new Date()));
        final File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        try {
            return File.createTempFile(fileName, suffix, storageDir);
        } catch (IOException e) {
            Log.e(TAG, "generateTempFile: ", e);
            return null;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static byte[] fromFile(File file) {
        try {
            int size = (int) file.length();
            byte[] bytes = new byte[size];

            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(bytes, 0, bytes.length);
            bufferedInputStream.close();

            return bytes;
        } catch (IOException e) {
            Log.e(TAG, "fromFile: ", e);
        }

        return new byte[0];
    }

    public static Intent createFileOpenIntent(String url) {
        final Intent viewFileIntent = new Intent(Intent.ACTION_VIEW);
        if (isGoogleDocsSupported(url)) url = String.format(DOCUMENT_URL, url);
        viewFileIntent.setData(Uri.parse(url));
        return viewFileIntent;
    }

    private static boolean isGoogleDocsSupported(String url) {
        return url.endsWith(".pdf") || url.endsWith(".doc") || url.endsWith(".docx")
                || url.endsWith(".ppt") || url.endsWith(".xls") || url.endsWith(".xlsx")
                || url.endsWith(".csv") || url.endsWith(".ods") || url.endsWith(".txt")
                || url.endsWith(".svg");
    }

}
