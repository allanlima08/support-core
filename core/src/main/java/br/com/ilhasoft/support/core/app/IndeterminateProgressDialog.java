package br.com.ilhasoft.support.core.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.ilhasoft.support.core.helpers.DimensionHelper;
import br.com.ilhasoft.support.core.helpers.ResourceHelper;
import me.zhanghai.android.materialprogressbar.IndeterminateProgressDrawable;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by daniel on 20/03/16.
 */
public class IndeterminateProgressDialog extends AlertDialog {

    private CharSequence message;
    private TextView textViewMesage;

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId) {
        return show(context, titleId, messageId, false);
    }

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId,
                                                   boolean cancelable) {
        return show(context, titleId, messageId, cancelable, null);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title,
                                                   CharSequence message) {
        return show(context, title, message, false);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title,
                                                   CharSequence message, boolean cancelable) {
        return show(context, title, message, cancelable, null);
    }

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId,
                                                   boolean cancelable,
                                                   @Nullable OnCancelListener cancelListener) {
        return show(context, ResourceHelper.getText(context, titleId),
                    ResourceHelper.getText(context, messageId), cancelable, cancelListener);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title,
                                                   CharSequence message, boolean cancelable,
                                                   @Nullable OnCancelListener cancelListener) {
        final IndeterminateProgressDialog dialog = new IndeterminateProgressDialog(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        dialog.show();
        return dialog;
    }

    public IndeterminateProgressDialog(Context context) {
        super(context);
    }

    public IndeterminateProgressDialog(Context context, @StyleRes int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setView(createLayout());
        if (message != null) setMessage(message);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setMessage(CharSequence message) {
        if (textViewMesage != null) {
            textViewMesage.setText(message);
        } else {
            this.message = message;
        }
    }

    private View createLayout() {
        final int verticalPadding = DimensionHelper.toPx(getContext(), 24);
        final int horizontalPadding = DimensionHelper.toPx(getContext(), 16);
        final LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);
        linearLayout.setPadding(horizontalPadding, verticalPadding,
                                horizontalPadding, verticalPadding);

        final MaterialProgressBar progressBar = new MaterialProgressBar(getContext());
        progressBar.setIndeterminateDrawable(new IndeterminateProgressDrawable(getContext()));
        progressBar.setIndeterminate(true);

        textViewMesage = new AppCompatTextView(getContext());
        textViewMesage.setPadding(horizontalPadding, 0, 0, 0);
        textViewMesage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        linearLayout.addView(progressBar, LinearLayout.LayoutParams.WRAP_CONTENT,
                             LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.addView(textViewMesage, LinearLayout.LayoutParams.WRAP_CONTENT,
                             LinearLayout.LayoutParams.WRAP_CONTENT);

        return linearLayout;
    }

}
