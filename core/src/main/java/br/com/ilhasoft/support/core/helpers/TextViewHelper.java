package br.com.ilhasoft.support.core.helpers;

import android.widget.TextView;

/**
 * Created by daniel on 26/05/16.
 */
public final class TextViewHelper {

    private TextViewHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static String getString(TextView textView) {
        return textView.getText().toString().trim();
    }

}
