package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;

/**
 * Created by john-mac on 5/25/16.
 */
public final class ColorHelper {

    private ColorHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    public static String getColorHexByResource(Context context, @ColorRes int colorRes) {
        return Integer.toHexString(ContextCompat.getColor(context, colorRes) & 0x00ffffff);
    }

    public static int darkColor(int color) {
        return darkColor(color, 0.8f);
    }

    public static int darkColor(int color, float offset) {
        float[] hsv = new float[3];

        Color.colorToHSV(color, hsv);
        hsv[2] *= offset;

        return Color.HSVToColor(hsv);
    }

}
