package br.com.ilhasoft.support.core.app;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.EditText;

import br.com.ilhasoft.support.core.helpers.DimensionHelper;
import br.com.ilhasoft.support.core.helpers.ResourceHelper;

/**
 * Created by daniel on 11/04/16.
 */
public class EditTextDialog extends AlertDialog {

    private EditText editText;
    private final DialogParams params;

    public static EditTextDialog show(Context context, @StringRes int title,
                                      @StringRes int hintText, @StringRes int positiveText,
                                      @StringRes int negativeText, boolean dismissOnCLick,
                                      OnInputTextListener onInputTextListener) {
        return show(context, ResourceHelper.getText(context, title),
                                   ResourceHelper.getText(context, hintText),
                                   ResourceHelper.getText(context, positiveText),
                                   ResourceHelper.getText(context, negativeText),
                                   dismissOnCLick, onInputTextListener);
    }

    public static EditTextDialog show(Context context, CharSequence title, CharSequence hintText,
                                      CharSequence positiveText, CharSequence negativeText,
                                      boolean dismissOnCLick,
                                      OnInputTextListener onInputTextListener) {
        final DialogParams params = new DialogParams();
        params.title = title;
        params.hintText = hintText;
        params.positiveText = positiveText;
        params.negativeText = negativeText;
        params.dismissOnCLick = dismissOnCLick;
        params.onInputTextListener = onInputTextListener;

        return new Builder(context, -1, params).show();
    }

    protected EditTextDialog(Context context, DialogParams params) {
        super(context);
        this.params = params;
    }

    protected EditTextDialog(Context context, int theme, DialogParams params) {
        super(context, theme);
        this.params = params;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this.getContext();
        editText = new AppCompatEditText(context);
        editText.setHint(params.hintText);

        final int spacing = DimensionHelper.toPx(context, 16);
        setView(editText, spacing, spacing / 2, spacing, spacing / 2);
        setTitle(params.title);
        setCancelable(true);
        setButton(BUTTON_POSITIVE, params.positiveText, onClickEmpty);
        setButton(BUTTON_NEGATIVE, params.negativeText, onClickEmpty);
        super.onCreate(savedInstanceState);

        getButton(BUTTON_POSITIVE).setOnClickListener(onClickOK);
        getButton(BUTTON_NEGATIVE).setOnClickListener(onClickCancel);
    }

    private final View.OnClickListener onClickOK = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (params.onInputTextListener != null) {
                params.onInputTextListener.onInputText(EditTextDialog.this, BUTTON_POSITIVE,
                                                editText.getText());
            }
            if (params.dismissOnCLick) {
                dismiss();
            }
        }
    };

    private final View.OnClickListener onClickCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (params.onInputTextListener != null) {
                params.onInputTextListener.onInputText(EditTextDialog.this, BUTTON_NEGATIVE, "");
            }
            dismiss();
        }
    };

    private final OnClickListener onClickEmpty = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) { }
    };

    public interface OnInputTextListener {
        void onInputText(EditTextDialog dialog, int which, CharSequence text);
    }

    protected static final class DialogParams {
        private CharSequence title;
        private CharSequence hintText;
        private CharSequence positiveText;
        private CharSequence negativeText;
        private boolean dismissOnCLick = true;
        private OnInputTextListener onInputTextListener;
    }

    public static final class Builder {
        @StyleRes
        private int theme;
        private final Context context;
        private final DialogParams params;
        public Builder(Context context) {
            this(context, -1);
        }
        public Builder(Context context, @StyleRes int theme) {
            this(context, theme, new DialogParams());
        }
        protected Builder(Context context, @StyleRes int theme, DialogParams params) {
            this.theme = theme;
            this.params = params;
            this.context = context;
        }
        public Builder setTitle(@StringRes int titleId) {
            params.title = ResourceHelper.getText(context, titleId);
            return this;
        }
        public Builder setTitle(CharSequence title) {
            params.title = title;
            return this;
        }
        public Builder setHintText(@StringRes int hintTextId) {
            params.hintText = ResourceHelper.getText(context, hintTextId);
            return this;
        }
        public Builder setHintText(CharSequence hintText) {
            params.hintText = hintText;
            return this;
        }
        public Builder setDismissOnCLick(boolean dismissOnCLick) {
            params.dismissOnCLick = dismissOnCLick;
            return this;
        }
        public Builder setPositiveText(@StringRes int positiveTextId) {
            params.positiveText = ResourceHelper.getText(context, positiveTextId);
            return this;
        }
        public Builder setPositiveText(CharSequence positiveText) {
            params.positiveText = positiveText;
            return this;
        }
        public Builder setNegativeText(@StringRes int negativeTextId) {
            params.negativeText = ResourceHelper.getText(context, negativeTextId);
            return this;
        }
        public Builder setNegativeText(CharSequence negativeText) {
            params.negativeText = negativeText;
            return this;
        }
        public Builder setOnInputTextListener(OnInputTextListener onInputTextListener) {
            params.onInputTextListener = onInputTextListener;
            return this;
        }
        public EditTextDialog create() {
            final EditTextDialog editTextDialog;
            if (theme > 0) {
                editTextDialog = new EditTextDialog(context, theme, params);
            } else {
                editTextDialog = new EditTextDialog(context, params);
            }
            return editTextDialog;
        }
        public EditTextDialog show() {
            final EditTextDialog editTextDialog = this.create();
            editTextDialog.show();
            return editTextDialog;
        }
    }

}
