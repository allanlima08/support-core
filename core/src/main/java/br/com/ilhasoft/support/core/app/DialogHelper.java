package br.com.ilhasoft.support.core.app;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import br.com.ilhasoft.support.core.R;
import br.com.ilhasoft.support.core.helpers.ResourceHelper;

/**
 * Created by daniel on 23/05/16.
 */
public final class DialogHelper {

    private DialogHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void showQuestion(Context context, @StringRes int titleRes, @StringRes int messageRes,
                                    @Nullable DialogInterface.OnClickListener onClickListener) {
        showQuestion(context, ResourceHelper.getText(context, titleRes),
                                  ResourceHelper.getText(context, messageRes),
                                  onClickListener);
    }

    public static void showQuestion(Context context, @Nullable CharSequence title, @Nullable CharSequence message,
                                    @Nullable DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.yes, onClickListener);
        builder.setNegativeButton(R.string.no, onClickListener);
        if (!TextUtils.isEmpty(title)) builder.setTitle(title);
        if (!TextUtils.isEmpty(message)) builder.setMessage(message);
        builder.show();
    }

}
