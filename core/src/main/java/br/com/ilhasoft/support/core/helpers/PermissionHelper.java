package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Helper class to simplify permission checking and validation
 *
 * @author John Cordeiro
 */
public final class PermissionHelper {

    private PermissionHelper() {
        throw new UnsupportedOperationException("This is a pure static class.");
    }

    /**
     * Verify if all the permissions results has granted status
     * @param grantResults results received on {@link android.app.Activity#onRequestPermissionsResult(int, String[], int[])}
     * @return {@code true} if all results is granted
     */
    public static boolean allPermissionsGranted(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) return false;
        }
        return true;
    }

    /**
     * Verify if the application has one single permission granted
     * @param context context of application
     * @param permission one of Manifest permission {@link android.Manifest.permission}
     * @return {@code true} if the permission was granted
     */
    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

}
