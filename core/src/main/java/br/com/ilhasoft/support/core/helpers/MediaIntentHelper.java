package br.com.ilhasoft.support.core.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;

import br.com.ilhasoft.support.core.R;

/**
 * This class help starting media intents. It automatically checks the Android version and passes
 * the correct parameters depending on it.
 *
 * @author John Cordeiro
 */
public final class MediaIntentHelper {

    public static final int VIDEO_QUALITY = 1;
    public static final int VIDEO_DURATION_LIMIT = 20;

    private MediaIntentHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    /**
     * It works in the same way as {@link MediaIntentHelper#startIntentImageFromGallery(Activity, int, boolean)},
     * in this case starting from an Fragment
     *
     * @param fragment Instance of the fragment who will start the intent
     */
    public static void startIntentImageFromGallery(Fragment fragment, int requestCode, boolean multipleFiles) {
        Intent intent = getImageFromGalleryIntent(multipleFiles);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * Start the intent to get image from pictures gallery
     *
     * @param activity Instance of the activity who will start the intent
     */
    public static void startIntentImageFromGallery(Activity activity, int requestCode, boolean multipleFiles) {
        Intent intent = getImageFromGalleryIntent(multipleFiles);
        activity.startActivityForResult(intent, requestCode);
    }

    @NonNull
    private static Intent getImageFromGalleryIntent(boolean multipleFiles) {
        Intent intent = createMultipleFilesIntent(multipleFiles);
        intent.setType("image/*");
        return intent;
    }

    @NonNull
    private static Intent createMultipleFilesIntent(boolean multipleFiles) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, multipleFiles);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }
        return intent;
    }

    /**
     * Works in the same way as {@link MediaIntentHelper#startIntentFile(Activity, int, boolean)}
     * in this case starting from a Fragment
     * @param fragment Instance of the Fragment who will start the intent
     */
    public static void startIntentFile(Fragment fragment, int requestCode, boolean multipleFiles) {
        Intent documentIntent = getIntentFile(multipleFiles);
        fragment.startActivityForResult(Intent.createChooser(documentIntent,
                fragment.getString(R.string.prompt_file_upload)), requestCode);
    }

    /**
     * Start the intent to get any file in the user device
     * @param activity Instance of the Activity who will start the intent
     */
    public static void startIntentFile(Activity activity, int requestCode, boolean multipleFiles) {
        Intent documentIntent = getIntentFile(multipleFiles);
        activity.startActivityForResult(Intent.createChooser(documentIntent,
                activity.getString(R.string.prompt_file_upload)), requestCode);
    }

    @NonNull
    private static Intent getIntentFile(boolean multipleFiles) {
        Intent documentIntent = createMultipleFilesIntent(multipleFiles);
        documentIntent.setType("*/*");
        return documentIntent;
    }

    /**
     * Works in the same way as {@link MediaIntentHelper#startIntentVideoFromCamera(Activity, int, int)}, only
     * starting from a fragment
     * @param fragment Instance of the Fragment who will start the intent
     */
    public static void startIntentVideoFromCamera(Fragment fragment, int requestCode, int duration) {
        startIntentVideoFromCamera(fragment, VIDEO_QUALITY, duration, requestCode);
    }

    /**
     * Start the intent to record video from Camera with default parameters
     * {@link MediaIntentHelper#VIDEO_QUALITY} and {@link MediaIntentHelper#VIDEO_DURATION_LIMIT}
     *
     * @param activity Instance of an Activity who will start the intent
     */
    public static void startIntentVideoFromCamera(Activity activity, int requestCode, int duration) {
        startIntentVideoFromCamera(activity, VIDEO_QUALITY, duration, requestCode);
    }

    /**
     * Works in the same way as {@link MediaIntentHelper#startIntentVideoFromCamera(Activity, int, int, int)},
     * starting from a fragment
     *
     * @param fragment Instance of the fragment who will start the intent
     * @param videoQuality Video quality or video {@link MediaStore#EXTRA_VIDEO_QUALITY}
     * @param durationLimit Maximum duration in seconds for the recorded video {@link MediaStore#EXTRA_DURATION_LIMIT}
     *
     * @see MediaStore
     */
    public static void startIntentVideoFromCamera(Fragment fragment, int videoQuality, int durationLimit, int requestCode) {
        Intent videoIntent = getVideoFromCameraIntent(videoQuality, durationLimit);
        if (videoIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
            fragment.startActivityForResult(videoIntent, requestCode);
        }
    }

    /**
     * Start the intent to record video from Camera passing main parameters
     *
     * @param activity Instance of the activity who will start the intent
     * @param videoQuality Video quality or video {@link MediaStore#EXTRA_VIDEO_QUALITY}
     * @param durationLimit Maximum duration in seconds for the recorded video {@link MediaStore#EXTRA_DURATION_LIMIT}
     *
     * @see MediaStore
     */
    public static void startIntentVideoFromCamera(Activity activity, int videoQuality, int durationLimit, int requestCode) {
        Intent videoIntent = getVideoFromCameraIntent(videoQuality, durationLimit);
        if (videoIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(videoIntent, requestCode);
        }
    }

    @NonNull
    private static Intent getVideoFromCameraIntent(int videoQuality, int durationLimit) {
        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        videoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, videoQuality);
        videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, durationLimit);
        return videoIntent;
    }

    /**
     * Works in the same way as {@link MediaIntentHelper#startIntentImageFromCamera(Activity, String, int)},
     * in this case starting from a Fragment
     *
     * @param fragment Instance of the fragment who will start the intent
     * @return File containing the path for the image after take it from the camera
     * @throws IOException
     */
    public static File startIntentImageFromCamera(Fragment fragment, String providerAuthority, int requestCode) throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
            File pictureFile = IoHelper.createImageFilePath(fragment.getContext());
            Uri pictureUri = createUriForFile(fragment.getContext(), providerAuthority, pictureFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);

            fragment.startActivityForResult(takePictureIntent, requestCode);
            return pictureFile;
        }
        return null;
    }

    /**
     * Start the intent to get image from the camera
     * @param activity Instance of the activity who will start the intent
     * @return File containing the path for the image after take it from the camera
     * @throws IOException
     */
    public static File startIntentImageFromCamera(Activity activity, String providerAuthority, int requestCode) throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            File pictureFile = IoHelper.createImageFilePath(activity);
            Uri pictureUri = createUriForFile(activity, providerAuthority, pictureFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);

            activity.startActivityForResult(takePictureIntent, requestCode);
            return pictureFile;
        }
        return null;
    }

    private static Uri createUriForFile(Context context, String providerAuthority, File pictureFile) {
        Uri pictureUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pictureUri = FileProvider.getUriForFile(context, providerAuthority, pictureFile);
        } else {
            pictureUri = Uri.fromFile(pictureFile);
        }
        return pictureUri;
    }

}
