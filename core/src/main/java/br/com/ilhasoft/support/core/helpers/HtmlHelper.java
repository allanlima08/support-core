package br.com.ilhasoft.support.core.helpers;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by john-mac on 9/21/16.
 */
public final class HtmlHelper {

    private HtmlHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

}
