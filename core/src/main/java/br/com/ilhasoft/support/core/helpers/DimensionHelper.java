package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * @deprecated Use the dimension resource instead.
 *
 * Created by daniel on 23/11/15.
 */

@Deprecated
public final class DimensionHelper {

    private DimensionHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static int toPx(Context context, float dpValue) {
        return DimensionHelper.toPx(context.getResources(), dpValue);
    }

    public static int toPx(Resources resources, float dpValue) {
        return DimensionHelper.toPx(resources.getDisplayMetrics(), dpValue);
    }

    public static int toPx(DisplayMetrics displayMetrics, float dpValue) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, displayMetrics));
    }

}
